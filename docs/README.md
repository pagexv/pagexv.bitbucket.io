## Role navigation

![image-20210209142005596.63ff8834](resources/image-20210209142005596.63ff8834.png)

### Brainstorm workload

Database management

DevOps operation

Message Queue

Cache

Log collection

Load balance





## Technology stack



### Frontend

#### Android / IOS

#### :white_check_mark: React Native



#### Vue



### Backend

#### java+spring

#### python+flask

#### nodejs+express

#### :white_check_mark: go+beego

https://beego.me/



### Alogorithm

#### :white_check_mark: Pytorch





## Work pattern

![image-20210713092926302](resources/image-20210713092926302.png)

## Overall phase



### First phase

#### 1.Setup front end ui

Design link: https://miro.com/welcomeonboard/bHM5MVNscmxmRnFqNXNuZVF2dzQwME9GWERWMFZxUmRlSVVpNmQ1RzlzSVBRYTZMVGx6MXlwZDI3WVVyUmgybHwzMDc0NDU3MzYxMzUwMjk3ODIw





#### 2.Setup backend server

Backend document proposal



#### 3.Connect front end and backend in localhost



#### 4.Optional: Jekins



### Second phase

1. Integrate algorithm

### Pilot run

Release first product

### Alpha

### Beta



### Estimation



### Meeting time

Wednesday, Sunday @7 pm



### Communication tool

Google  meeting: https://meet.google.com/vox-cyqi-ieg

WeChat

HomePage: https://dengnaichen.github.io/#



## Retrospective

At end of two week.

## Week July 12 - 18

| Team member | What I accomplished                                          | Goal of next week                  | Blocks |
| ----------- | ------------------------------------------------------------ | ---------------------------------- | ------ |
| Hanfei      | Setup backend go<br />Write readme file<br />Research react android app | Complete swagger api               | No     |
| Naichen     | Learn react native, <br />Run sample code<br />Install Xcode | 环境配置<br />按钮出现跳转屏幕相机 | No     |



## Check point July 23





## 

